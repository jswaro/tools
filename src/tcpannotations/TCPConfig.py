import os
import os.path
import ConfigParser


LOGLABELS = frozenset(["TCP::Retransmissions", "TCP::Reordering", "TCP::Options",
                            "TCP::ConnectionFailure", "TCP::SpuriousRetransmission"]);
                            
DEFAULT_CONFIG_NAME = ".tcpcongconfig";
DEFAULT_CONFIG_PATH = "~";
ACCEPTED_CONFIG_PATH = None;
WRITE_UPON_EXIT = False;

def getConfigValue(configObj, section, index):
    value = None
    try:
        value = configObj.get(section, index);
    except ConfigParser.NoOptionError as err:
        value = None;
    finally:
        return value;

def validateConfig(configObj):
    defaultsearchdir = "";
    try:
        defaultsearchdir = configObj.get("Summary", "defaultsearchpath");
    except ConfigParser.NoSectionError as value:
        buildSummaryConfig(configObj);
        defaultsearchdir = configObj.get("Summary", "defaultsearchpath");
        WRITE_UPON_EXIT = True;
    except Exception as err:
        raise Exception(err);

    if( defaultsearchdir == None ):
        raise Exception("Error: default search path not set in configuration \
                            file");
    if( not os.path.isdir(defaultsearchdir) ):
        raise Exception("Error: default search path is not a valid directory \
                        \n    <Summary>: " + defaultsearchdir + " in config file");
    

def buildDefaultConfig(path):
    if( type(path) != str):
        return;
    path = os.path.expanduser(path);
    configFile = open(path, "w");

    config = ConfigParser.SafeConfigParser();

    buildSummaryConfig(config);

    config.write(configFile);

def buildSummaryConfig(configObj):
    configObj.add_section("Summary");
    configObj.set("Summary", "ConnectionSummary", "conn.log");
    configObj.set("Summary", "DeadConnLog", "tcpdeadconnection.log");
    configObj.set("Summary", "RetransmissionLog", "tcpretransmissions.log");
    configObj.set("Summary", "OptionsLog", "tcpoptions.log");
    configObj.set("Summary", "ReorderingLog", "tcpreordering.log");
    configObj.set("Summary", "RTTLog", "tcprtt.log");
    configObj.set("Summary", "defaultsearchpath", os.curdir);

def getConfig(configPath=None):
    config = ConfigParser.SafeConfigParser();
    if( configPath == None ):
        configAbsPath = os.path.expanduser(DEFAULT_CONFIG_PATH + os.sep + \
            DEFAULT_CONFIG_NAME);
    else:
        configAbsPath = os.path.expanduser(configPath);
    try:
        f = None;
        f = open(configAbsPath, "r");
    except IOError as e:
        if( configAbsPath != (DEFAULT_CONFIG_PATH + os.sep + DEFAULT_CONFIG_NAME)):
            print "Config file doesn't exist. Trying default ... "
        configAbsPath = DEFAULT_CONFIG_PATH + os.sep + DEFAULT_CONFIG_NAME;
        configAbsPath = os.path.expanduser(configAbsPath);
        try:
            f = None;
            f = open(configAbsPath, "r");
        except IOError as e:
            print "Default Config file doesn't exist. Building default ... "
            buildDefaultConfig( configAbsPath );
        finally:
            if( f != None):
                f.close();
    finally:
        if( f != None):
            f.close();
            
    ACCEPTED_CONFIG_PATH = configAbsPath;

    config.read(configAbsPath);
    try:
        validateConfig(config);
    except Exception as err:
        print str(err);
        sys.exit(2);

    return config;

def writeConfigToFile(configObj):
    if( not ACCEPTED_CONFIG_PATH == None ):
        if( os.path.isfile(ACCEPTED_CONFIG_PATH)):
            configObj.write(ACCEPTED_CONFIG_PATH);
        else:
            raise Exception("Error: configuration file path does not specify a \
                            file");
