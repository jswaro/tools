import sys

class BroLog: 
    def __init__(self, filename):
        self.logfile = None;
        self.fields = None;
        
        try:
            self.logfile = open(filename, "r");
        except IOError as (errno, strerror):
            print "Warning: unable to open ", filename, " for processing...";
            self.logfile = None;
            return;
        
        try:
            self.logfile.readline(); #seperator line
            self.logfile.readline(); #set-separator line
            self.logfile.readline(); #empty-field line
            self.logfile.readline(); #unset field
            self.logfile.readline(); #path line
            self.logfile.readline(); #open line
            
            self.fields = self.logfile.readline().rstrip().split();
            
            self.logfile.readline(); #types line
            
        except:
            print "Unexpected error in processing log header";
            sys.exit(0);
        
    def good(self):
        return self.logfile != None

    def getFields(self):
        return self.fields;

    def readline(self):
        record = self.logfile.readline();
        
        if(record.split()[0] == "#close"):
            self.close();
        return record;
    
    def close(self):
        if (self.logfile != None):
            self.logfile.close();
        self.logfile = None;
    