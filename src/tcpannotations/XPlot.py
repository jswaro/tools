import CongestionLogs
from CongestionLogs import CATEGORY_FAST, RETRANSMISSION_TYPES, CATEGORY_RTO, \
                    CATEGORY_UNKNOWN, CONNECTION_STATE

xplotTypes = frozenset(["uarrow","darrow" ,"utick" ,"dtick" ,"line"])


ONLYEVENT=False

def GetOrig(entry, labelIndex):
    if( labelIndex == None ):
        return;

    data = entry.rstrip().split()

    label = data[labelIndex];

    if( label == "TCP::Retransmissions" ):
        return CongestionLogs.RetransmissionRecord(data).orig;
    elif( label == "TCP::Reordering" ):
        return CongestionLogs.ReorderingRecord(data).orig;
    elif( label == "TCP::Options" ):
        pass;
    elif( label == "TCP::ConnectionFailure"):
        return CongestionLogs.ConnectionFailureRecord(data).orig;
    elif( label == "TCP::SpuriousRetransmission"):
        return CongestionLogs.RetransmissionRecord(data).orig;
    else:
        raise Exception("Label Unrecognized: " + label);
    return;


class xpl:
    def __init__(self, filename=None, hash=None, TCPID=None, types=None,title=None, xlabel=None,
            ylabel=None):
        self.filename = filename;
        self.hash = hash;
        self.TCPID = TCPID;
        self.types = types;
        self.title = title;
        self.xlabel = xlabel;
        self.ylabel = ylabel;
        self.ts_min = float(0);
        self.ts_max = float(0);
        self.seq_min = long(0);
        self.seq_max = long(0);
        self.last_seq = long(0);
        self.processedevents = False;
        self.lines = [];



    def ProcessRetransmissionRecord(self, data):
        record = CongestionLogs.RetransmissionRecord(data);

        rtx_type = record.retransmissiontype;
        ts = record.timestamp;
        seq = record.seq;
        self.last_seq = seq;
    
        seq_max = 1 << 32;
        label = None;

        color = "red";
        textloc = "btext"

        label = rtx_type;

        if( label == "LIMITEDTRANSMIT" ):
            label = "RFC3042";
        if( label == "FASTRECOVERY" ):
            label = "RFC5681";
            
        if ONLYEVENT:
            t = "EVENT"

        print rtx_type, record

        if( rtx_type in RETRANSMISSION_TYPES):

            if( rtx_type in ("FastRTX", "RTO", "FACKRexmit", "SpuriousRexmit",
                    "UNKNOWN_REXMIT_TYPE")):
                if( rtx_type == "SpuriousRexmit" ):
                    textloc = "atext";
                color = "blue";#"red";
            elif(rtx_type in ("OutOfOrder","Ambiguous_OutOfOrder")):
                color = "blue";
            else:
                color = "blue";#"green";

            color = "red";
            output_string = textloc + " " + str(ts) + " " + str(seq).split("L")[0];
            #if ts >= self.ts_min and ts <= self.ts_max and seq >= self.seq_min and seq <= self.seq_max:
            self.lines.append(output_string + " " + color + "\n" + label +"\n");
            #else:
            #    print data
            #    print "line is out of bounds";

    def ProcessReorderingRecord(self, data):
        record = CongestionLogs.ReorderingRecord(data);
        
        t = "reordering"
        if record.ambiguous:
            t = "ambiguous"
        ts = record.timestamp;
        seq = record.seq;

        color = "blue";
        textloc = "btext"

        if ONLYEVENT:
            t = "EVENT"

        label = t;
        output_string = textloc + " " + str(ts) + " " + str(seq).split("L")[0];
        self.lines.append(output_string + " " + color + "\n" + label +"\n");

    def ProcessSpuriousRetransmissionRecord(self, data):
        record = CongestionLogs.RetransmissionRecord(data);

    def ProcessConnectionFailureRecord(self, data, seq):
        record = CongestionLogs.ConnectionFailureRecord(data);

        ts = record.timestamp;
        label = "DEADCONN";

        if ONLYEVENT:
            t = "EVENT"

        color = "blue";
        textloc = "btext"
        output_string = textloc + " " + str(ts) + " " + str(seq).split("L")[0];
        self.lines.append(output_string + " " + color + "\n" + label +"\n");

    def ProcessLine(self, entry, labelIndex):
        if( labelIndex == None ):
            return;
        
        split = entry.rstrip().split()
        
        label = split[labelIndex];

        if( label == "TCP::Retransmissions" ):
            self.processedevents = True;
            self.ProcessRetransmissionRecord(split);
        elif( label == "TCP::Reordering" ):
            self.processedevents = True;
            self.ProcessReorderingRecord(split);
        elif( label == "TCP::Options" ):
            pass;
        elif( label == "TCP::ConnectionFailure"):
            self.processedevents = True;
            self.ProcessConnectionFailureRecord(split, self.last_seq);
        elif( label == "TCP::SpuriousRetransmission"):
            self.processedevents = True;
            self.ProcessSpuriousRetransmissionRecord(split);
        else:
            raise Exception("Label Unrecognized: " + label);
                
        return;

    def Append(self, line):
        if( line.rstrip() != "go"):
            self.lines.append(line);

        try:
            rtx_type = line.split()[0]
            if rtx_type in xplotTypes:
                ts  = float(line.split()[1])
                seq = long(line.split()[2])
                if self.ts_min == 0 or self.ts_min > ts:
                    self.ts_min = ts
                if self.ts_max == 0 or self.ts_max < ts:
                    self.ts_max = ts
                if self.seq_min == 0 or self.seq_min > seq:
                    self.seq_min = seq
                if self.seq_max == 0 or self.seq_max < seq:
                    self.seq_max = seq
        except IndexError:
            pass;

    def toString(self):
        ret = "";
        ret +=  self.types + \
                "title\n" + \
                self.title + \
                "xlabel\n" + \
                self.xlabel + \
                "ylabel\n" + \
                self.ylabel;
        for entry in self.lines:
            ret += entry;
        ret += "go\n";

        return ret;
