from decimal import *
import CongestionLogs
from CongestionLogs import CATEGORY_FAST, RETRANSMISSION_TYPES, CATEGORY_RTO, \
                    CATEGORY_UNKNOWN, CONNECTION_STATE

class ConnectionAnnotation:
    def __init__(self, TCPID=None):
        self.TCPID = TCPID;
        self.events = [];

class ConnectionSummary:
    def __init__(self, data):
        split = data.rstrip().split();
        self.timestamp = split[0];
        self.src_ip = split[2].rstrip();
        self.src_port = split[3].rstrip();
        self.dst_ip = split[4].rstrip();
        self.dst_port = split[5].rstrip();
        
        self.src_is_orig = True;
        
        self.num_rexmit = 0;
        self.num_fast_rexmit = 0;
        self.num_rto	= 0;
        self.num_early_rexmit = 0;
        self.num_fack   = 0;
        self.num_reordered = 0;
        self.num_gap_check = 0;
        self.num_ambiguous = 0;
        self.num_spurious = 0;
        self.num_rexmit_events = 0;
        self.spurious_toward_orig = 0;
        self.spurious_toward_resp = 0;
        self.spurious_threshold_count = 0;
        
        self.num_conn_deaths = 0;
        self.total_time_dead = Decimal("0");
        self.longest_time_dead = Decimal("0");
        
        self.rowts_rtt = 0;
        self.reordered_without_ts = 0
        self.rowts_gap = 0;
        self.rowts_seg_diff = 0;
        
        self.rto = StateList();
        self.fastrtx = StateList();
        self.deadconn = StateList();
        self.fack = StateList();
        
        self.rexmit_types = RexmitList();
        
        self.reordered_with_ts = 0;
        
        self.reordered_to_outside = 0;
        self.reordered_from_outside = 0;
        
        self.valid = True;
        self.sack_enabled = False;
        self.client_sack_permitted = False;
        self.server_sack_permitted = False;
        
        self.timestamps_enabled = False;
        self.isAlive = True;
        
        self.unknown_type = 0;
        self.num_brute = 0;
        
        self.events = [];
        
    def Append(self, data):
        self.events.append(data);

    def ProcessRetransmissionRecord(self, data, append):
        record = CongestionLogs.RetransmissionRecord(data);

        if ( append ):
            self.Append(record);

        self.num_rexmit += 1;
        if( record.retransmissiontype in CATEGORY_FAST ):
            self.num_fast_rexmit += 1;
            self.num_rexmit_events += 1;
            self.fastrtx.add(record.state);
        elif( record.retransmissiontype in CATEGORY_RTO ):
            if( record.retransmissiontype == "BRUTE_FORCE_RTO" ):
                self.num_brute += 1;
            else:
                self.num_rto += 1;
                self.num_rexmit_events += 1;
                self.rto.add(record.state);
        elif( record.retransmissiontype in CATEGORY_UNKNOWN ):
            self.unknown_type += 1;
            self.num_rexmit_events += 1;
        else:
            #must be a basic retransmit
            pass;


    def ProcessReorderingRecord(self, data, append):
        
        record = CongestionLogs.ReorderingRecord(data);

        if ( append ):
            self.Append(record);

        if( record.ambiguous ):
            self.num_ambiguous += 1;
        else:
            self.num_reordered += 1;
            if(float(record.gap) > 0):
                    #this was a Out of Order packet found by gap analysis
                    self.rowts_rtt += record.rtt;
                    self.reordered_without_ts += 1;
                    self.rowts_gap += record.gap;
                    self.rowts_seg_diff += record.segments_outoforder;
            else:
                    self.reordered_with_ts += 1

        

    def ProcessOptionsRecord(self, data, append):
        record = CongestionLogs.OptionsRecord(data);

        if ( append ):
            self.Append(record);

        self.timestamps_enabled = record.timestamps;
        self.valid = not record.bad;
        self.sack_enabled = record.sack_used;
        self.client_sack_permitted = record.orig_sack_offer;
        self.server_sack_permitted = record.resp_sack_offer;

    def ProcessConnectionFailureRecord(self, data, append):
        record = CongestionLogs.ConnectionFailureRecord(data);

        if ( append ):
            self.Append(record);

        self.num_conn_deaths += 1;
        self.total_time_dead += record.duration;
        if( self.longest_time_dead < record.duration ):
            self.longest_time_dead = record.duration;
        self.deadconn.add(record.state);

    def ProcessSpuriousRetransmissionRecord(self, data, append):
        record = CongestionLogs.RetransmissionRecord(data);

        if ( append ):
            self.Append(record);

        self.num_spurious += 1;

    def ProcessLine(self, line, labelIndex, append):

        if( labelIndex == None ):
            return;

        split = line.rstrip().split()
        
        label = split[labelIndex];

        if( label == "TCP::Retransmissions" ):
            self.ProcessRetransmissionRecord(split, append);
        elif( label == "TCP::Reordering" ):
            self.ProcessReorderingRecord(split, append);
        elif( label == "TCP::Options" ):
            self.ProcessOptionsRecord(split, append);
        elif( label == "TCP::ConnectionFailure"):
            self.ProcessConnectionFailureRecord(split, append);
        elif( label == "TCP::SpuriousRetransmission"):
            self.ProcessSpuriousRetransmissionRecord(split, append);
        else:
            raise Exception("Label Unrecognized: " + label);
        return;

    def IsAlive(self):
        return self.isAlive;

    def UsesTimestamps(self):
        return self.timestamps_enabled;

    def UsesSACK(self):
        return self.sack_enabled;

    def ProcessSpurious(self):
        for e in self.events:
            split = e.rstrip().split();
            type = split[1];
            if( type == "SpuriousRexmit" ):
                rtype = "Unknown";
                seq_number = split[4];
                time = split[0]
                for l in self.events:
                    search_line = l.rstrip().split();
                    if( search_line[1] == "SpuriousRexmit" or
                        search_line[1] == "OutOfOrder" or
                        search_line[1] == "Ambiguous_OutOfOrder"):
                        continue;
                    if( search_line[4] == seq_number ):
                        rtype = search_line[1];
                        break;
                    if( search_line[4] == time ):
                        rtype = search_line[1];
                        break;


                if( rtype == "FastRTX" ):
                    self.rexmit_types.fastrtx += 1;
                elif( rtype == "RTO" ):
                    self.rexmit_types.rto += 1;
                elif( rtype == "UNKNOWN_REXMIT_TYPE" ):
                    self.rexmit_types.unknown += 1;
                elif( rtype == "FACKRexmit" ):
                    self.rexmit_types.fack += 1;
                elif(rtype == "REXMIT"):
                    #this is a really rare edge case
                    pass;
                else:
                    print rtype;
                    print e;
                    for p in self.events:
                        print p.rstrip();
                    print "\n\n";

class RexmitList:
    def __init__(self):
        self.rto = 0;
        self.fastrtx = 0;
        self.fack = 0;
        self.unknown = 0;

class StateList:
    def __init__(self):
        self.state = {};
        for type in CONNECTION_STATE:
            self.state[type] = 0;
    
    def add(self, type):
        if( type in CONNECTION_STATE ):
            self.state[type] += 1;
        else:
            print "Error: ", type, " is an undefined type."

    def toString(self, prefix):
        getcontext.prec = 2;
        total = 0;
        for type in CONNECTION_STATE:
            total += self.state[type];
        toReturn = "";

        total = Decimal(total);
        for type in CONNECTION_STATE:

            if( total  == Decimal('0') ):
                percent = Decimal('0');
            else:
                percent = Decimal('100') * (Decimal(self.state[type]) / total);

            toReturn += ("  {0:16}: {1:10} ({2:6}%)\n").format( \
                    str(type).rjust(16), \
                    repr(self.state[type]).rjust(10), \
                    str(percent).rjust(6));
        return toReturn;

    def dump(self, of):
        ret = "(";
        for type in self.state:
            ret += repr(self.state[type]) + "\t"
        ret += ")"
        return ret;

def GetDuration(str):
    t = str.rstrip().split('=')
    duration = float(t[1])
    return duration

def HasRTX(str):
    t = str.rstrip().split('=')
    rtx = int(t[1])
    if( rtx > 0):
        return 1
    return 0

def IsAlive(str):
    t = str.rstrip().split('=')
    alive = (t[1] == "ALIVE")
    return alive

def OutputPercent(str, x, total, outfile):
    if( x > 0 and total > 0):
        outfile.write(str + repr(x) + "   \t(" + GetPct(repr(round(float(x * 100) / float(total), 2))) + " %)\n")

def GetStat(str, x, total):
    s = str
    try:
        s += repr(x) + "   \t(" + GetPct(repr(round(float(x * 100) / float(total),2))) + " %)\n"
    except ZeroDivisionError:
        s = "\n"
    return s

def GetPct(d):
    getcontext().prec = 3
    to_round = Decimal(d)
    str = repr(+to_round).rstrip().split('\'')
    return str[1]

def GetNum(d):
    getcontext().prec = 2
    to_round = Decimal(d)
    str = repr(+to_round).rstrip().split('\'')
    return str[1]

def AddStates(x, y):
    for type in CONNECTION_STATE:
        x.state[type] += y.state[type];
