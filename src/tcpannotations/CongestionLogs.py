from decimal import *
from time import *

RETRANSMISSION_TYPES = frozenset([ "UNKNOWN", "RTO", "FAST_3DUP", "FAST_SUSPECT",
    "EARLY_REXMIT", "REXMIT", "TESTING", "NO_RTT", "NO_TS", "SEGMENT_EARLY_REXMIT",
    "BYTE_EARLY_REXMIT", "SACK_SEGMENT_EARLY_REXMIT", "SACK_BYTE_EARLY_REXMIT",
    "SACK_BASED_RECOVERY", "BRUTE_FORCE_RTO", "RTO_NO_DUP_ACK",
    "FACK_BASED_RECOVERY"]);

CATEGORY_FAST = frozenset(["FAST_3DUP", "FAST_SUSPECT",
    "EARLY_REXMIT", "SEGMENT_EARLY_REXMIT",
    "BYTE_EARLY_REXMIT", "SACK_SEGMENT_EARLY_REXMIT", "SACK_BYTE_EARLY_RECOVERY",
    "SACK_BASED_RECOVERY", "FACK_BASED_RECOVERY"]);

CATEGORY_RTO = frozenset(["RTO", "RTO_NO_DUP_ACK", "BRUTE_FORCE_RTO"]);

CATEGORY_UNKNOWN = frozenset(["UNKNOWN", "TESTING", "NO_RTT", "NO_TS"]);

CONNECTION_STATE = frozenset(["UNKNOWN", "3WHS", "SS", "CA", "CLOSE", "LIMITED",
    "STEADY", "ZEROWINDOW", "REPEATING", "IDLE"]);


__state_lookup = {0: "UNKNOWN", 1: "3WHS", 2: "SS", 3: "CA", 4: "CLOSE", 
                  5: "LIMITED", 6: "STEADY", 7: "ZEROWINDOW", 8: "REPEATING", 
                  9: "IDLE"};

__reason_lookup = {0: "UNKNOWN", 1: "PREV_OBSERVED", 2: "SPANS_PREV", 
                   3: "PREV_ACKED", 4: "PARTIALLY_ACKED", 5: "TIMESTAMP", 
                   6: "ACK", 7: "GAP"};
                 
__retransmissiontype_lookup = {0: "UNKNOWN", 1: "RTO", 2: "FAST_3DUP", 
                               3: "FAST_SUSPECT", 4: "EARLY_REXMIT", 5: "REXMIT",
                               6: "TESTING", 7: "NO_RTT", 8: "NO_TS", 
                               9: "SEGMENT_EARLY_REXMIT", 10: "BYTE_EARLY_REXMIT", 
                               11: "SACK_SEGMENT_EARLY_REXMIT", 
                               12: "SACK_BYTE_EARLY_REXMIT", 
                               13: "SACK_BASED_RECOVERY", 14: "BRUTE_FORCE_RTO", 
                               15: "RTO_NO_DUP_ACK", 16: "FACK_BASED_RECOVERY"};
__flags_lookup = {0: "None",
                  1: "FIN",
                  2: "SYN",
                  4: "RST"};

__data_definitions = {"timestamp":"Decimal", "uid":"string", "orig_h":"string", 
                      "orig_p":"string", "resp_h":"string", "resp_p":"string",
                      "label":"string", "seq":"int", "rtt":"Decimal", "rto":"Decimal",
                      "state":"lu.string", "orig":"bool", "reason":"lu.string",
                      "retransmissiontype":"lu.string", "confidence":"Decimal",
                      "nnseq":"int", "flags":"lu.int", "duration":"Decimal",
                      "gap":"Decimal", "segments_outoforder":"int", 
                      "ambiguous":"bool", "timestamps":"bool", "bad":"bool",
                      "sack_used":"bool", "orig_sack_offer":"bool",
                      "resp_sack_offer":"bool" };

RetransmissionFields = ["timestamp", "uid", "orig_h", "orig_p", "resp_h", 
                        "resp_p", "label", "seq", "rtt", "state", "orig",
                        "reason", "retransmissiontype", "confidence", 
                        "nnseq", "flags"];
                        
ConnFailureFields = ["timestamp", "uid", "orig_h", "orig_p", "resp_h", 
                     "resp_p", "label", "duration", "state", "orig"];
            
ReorderingFields = ["timestamp", "uid", "orig_h", "orig_p", "resp_h", 
                    "resp_p", "label", "seq", "gap", "rtt",
                    "segments_outoforder", "orig", "ambiguous", "nnseq"];

OptionsFields = ["timestamp", "uid", "orig_h", "orig_p", "resp_h", 
                 "resp_p", "label", "timestamps", "bad", "sack_used", 
                 "orig_sack_offer", "resp_sack_offer"];
                 
RecoveryFields = ["timestamp", "uid", "orig_h", "orig_p", "resp_h", 
                  "resp_p", "label", "seq", "rtt", "state", "orig", "nnseq"];
                  
RTTFields = ["timestamp", "uid", "orig_h", "orig_p", "resp_h", "resp_p", "label",
             "rtt", "rto", "orig"];


''' converts values from bro log datatypes to python data types '''
def convert_datatype(label, value):
    global __data_definitions;
    
    if (label not in __data_definitions):
        raise Exception("{0:s} does not exist in data definitions".format(label));
    
    if (value == "-"):
        return "-";
    
    data_type = __data_definitions[label].split(".")[0];
    if (data_type == "Decimal"):
        return Decimal(value);
    elif (data_type == "string"):
        return str(value);
    elif (data_type == "int"):
        return int(value);
    elif (data_type == "bool"):
        return value == "T";
    elif (data_type == "lu"):
        return lookup_value_id(label, int(value));

    raise Exception("Unknown data type: {0:s}".format(data_type));
    return None;

''' looks up a value from one of the cables and converts as necessary ''' 
def lookup_value_id(category, value):
    global __reason_lookup;
    global __retransmissiontype_lookup;
    global __state_lookup;
    
    table = None;
    if (category == "reason"):
        table = __reason_lookup;
    elif (category == "retransmissiontype"):
        table = __retransmissiontype_lookup;
    elif (category == "state"):
        table = __state_lookup;
    elif (category == "flags"):
        table = __flags_lookup;
    else:
        raise Exception("Bad Category: {0:s}".format(category));
    
    if (not value in table):
        raise Exception("Bad value for table {0:s}: {1:u}".format(category, value));
    return table[value];

class LogRecord:
    def __init__(self, fields, record_definition):
        label = ""
        value = ""
        try:
            for x in xrange(0,len(record_definition)):
                label = record_definition[x];
                value = fields[x];
                setattr(self, label, convert_datatype(label, value));
        except Exception as e:
            print e, label, value;
            raise e

    def toString(self, fields):
        ret = "";
        
        for label in fields:
            ret += "{0:s}\t".format(str(getattr(self, label)));
        
        return ret.rstrip();
        

class RetransmissionRecord(LogRecord):
    def __init__(self, fields):
        LogRecord.__init__(self, fields, RetransmissionFields);

    def toString(self):
        return LogRecord.toString(self, RetransmissionFields);

class ConnectionFailureRecord(LogRecord):
    def __init__(self, fields):
        LogRecord.__init__(self, fields, ConnFailureFields);

    def toString(self):
        return LogRecord.toString(self, ConnFailureFields);

class ReorderingRecord(LogRecord):
    def __init__(self, fields):
        LogRecord.__init__(self, fields, ReorderingFields);

    def toString(self):
        return LogRecord.toString(self, ReorderingFields);

class OptionsRecord(LogRecord):
    def __init__(self, fields):
        LogRecord.__init__(self, fields, OptionsFields);

    def toString(self):
        return LogRecord.toString(self, OptionsFields);

class RecoveryRecord(LogRecord):
    def __init__(self, fields):
        LogRecord.__init__(self, fields, RecoveryFields);

    def toString(self):
        return LogRecord.toString(self, RecoveryFields);
    
class RTTRecord(LogRecord):
    def __init__(self, fields):
        LogRecord.__init__(self, fields, RTTFields);

    def toString(self):
        return LogRecord.toString(self, RTTFields);
    
def test():
    try:
        retransmission_record = "1285862196.756494       tQ5d7DNwsca     10.0.0.8        2944    192.168.0.7     80      TCP::Retransmissions    1765207089      0.0     1       F       1       1       -       1765207089      2";    
        values = retransmission_record.split();
        tester = RetransmissionRecord(values);
        print tester.toString();
    except Exception as e:
        print "failed RetransmissionRecord"
        print e;
        
    try:
        connection_failure = "1285862198.014044       POo8hxjJK0h     10.0.0.124      4064    192.168.0.3     80      TCP::ConnectionFailure  1.022474        1       T"
        values = connection_failure.split();
        tester = ConnectionFailureRecord(values);
        print tester.toString();
    except Exception as e:
        print "failed ConnectionFailureRecord"
        print e;
        
    try:
        reordering_record = "1285862197.283803       87f9SK7nyXc     10.0.0.158      57168   192.168.0.22    80      TCP::Reordering 4083290527      0.000088        0.038876        1       T       -       4083290526"
        values = reordering_record.split();
        tester = ReorderingRecord(values);
        print tester.toString();
    except Exception as e:
        print "failed ReorderingRecord";
        print e;
        
    try:
        options_record = "1285862201.345246       ey4oIThTBH      10.0.0.23       54352   192.168.0.6     80      TCP::Options    F       T       F       F       F"
        values = options_record.split();
        tester = OptionsRecord(values);
        print tester.toString();
    except Exception as e:
        print "failed OptionsRecord";
        print e;
        
    try:
        recovery_record = "1285862213.789876       8ePv9Mre0Cb     10.0.5.113      50848   192.168.0.17    80      TCP::FASTRECOVERY       2032617453      0.131769        2       F       2032600292"
        values = recovery_record.split();
        tester = RecoveryRecord(values);
        print tester.toString();
    except Exception as e:
        print "failed RecoveryRecord"
        print e;
        
    try:
        rtt_record = "1285862196.428476       ID8L9aIPAZf     10.0.0.35       38032   192.168.0.4     80      TCP::InitialRTT 0.022751        -    T"
        values = rtt_record.split();
        tester = RTTRecord(values);
        print tester.toString();
    except Exception as e:
        print "failed RTTRecord"
        print e;
    return
    
if (__name__ == "__main__"):
    test();
