from decimal import *
import os
from time import strftime
import sys
import time
import random
import commands
import shlex
import subprocess
import time

class Connection:
    def __init__(self, data):
        self.list = []

        split = data.rstrip().split()
        self.timestamp = split[0]
        self.src_ip = split[2]
        self.src_port = split[3]
        self.dst_ip = split[4]
        self.dst_port = split[5]

class StateList:
    def __init__(self):
        self.ThreeWHS = 0
        self.ConnClose = 0
        self.Unknown = 0
        self.SlowStart = 0
        self.FastRTX = 0
        self.CongAvoid = 0
        self.WindowLimited = 0
        self.Steady = 0
        self.ZeroWindow = 0
        self.RepeatRTX = 0
        self.Idle = 0

def IncreaseCount(str, statelist):
    t = str.rstrip().split('=')
    type = t[1]
    if type == "3WHS":
        statelist.ThreeWHS += 1
    elif type == "Slow_Start":
        statelist.SlowStart += 1
    elif type == "Fast_Retransmit":
        statelist.FastRTX += 1
    elif type == "Congestion_Avoidance":
        statelist.CongAvoid += 1
    elif type == "Connection_Close":
        statelist.ConnClose += 1
    elif type == "Window_Limited":
        statelist.WindowLimited += 1
    elif type == "SteadyState":
        statelist.Steady += 1
    elif type == "Zero_Window":
        statelist.ZeroWindow += 1
    elif type == "Repeating_RTX":
        statelist.RepeatRTX += 1
    elif type == "Idle":
        statelist.Idle += 1
    else:
        statelist.Unknown += 1

def PrintStats(str, statelist, outfile):
    total = statelist.ThreeWHS + statelist.SlowStart + statelist.FastRTX
    total += statelist.CongAvoid + statelist.ConnClose + statelist.WindowLimited
    total += statelist.Steady + statelist.ZeroWindow + statelist.RepeatRTX
    total += statelist.Idle + statelist.Unknown
    to_print = str + '\n'
    to_print += GetStat("  3WHS     : " ,statelist.ThreeWHS     , total)#" 3WHS: " + repr(statelist.ThreeWHS)
    to_print += GetStat("  SS       : " ,statelist.SlowStart    , total)#" SS: " + repr(statelist.SlowStart)
   #to_print += GetStat("  FastRTX  : " ,statelist.FastRTX      , total)#" FastRTX: " + repr(statelist.FastRTX)
    to_print += GetStat("  CA       : " ,statelist.CongAvoid    , total)#" CA: " + repr(statelist.CongAvoid)
    to_print += GetStat("  Close    : " ,statelist.ConnClose    , total)#" Close: " + repr(statelist.ConnClose)
    to_print += GetStat("  WL       : " ,statelist.WindowLimited, total)#" WL: " + repr(statelist.WindowLimited) + "\n" + str
    to_print += GetStat("  Steady   : " ,statelist.Steady       , total)#" STEADY: " + repr(statelist.Steady)
    to_print += GetStat("  ZeroWin  : " ,statelist.ZeroWindow   , total)#" ZeroWin: " + repr(statelist.ZeroWindow)
    to_print += GetStat("  RepeatRTX: " ,statelist.RepeatRTX    , total)#" RepeatRTX: " + repr(statelist.RepeatRTX)
    to_print += GetStat("  Idle     : " ,statelist.Idle         , total)#" Idle: " + repr(statelist.Idle)
    to_print += GetStat("  Unknown  : " ,statelist.Unknown      , total)#" Unknown: " + repr(statelist.Unknown) + "\n"
    outfile.write(to_print)

def GetDuration(str):
    t = str.rstrip().split('=')
    duration = float(t[1])
    return duration

def HasRTX(str):
    t = str.rstrip().split('=')
    rtx = int(t[1])
    if( rtx > 0):
        return 1
    return 0

def IsAlive(str):
    t = str.rstrip().split('=')
    alive = (t[1] == "ALIVE")
    #print alive
    return alive

def OutputPercent(str, x, total, outfile):
    outfile.write(str + repr(x) + "   \t(" + GetPct(repr(round(float(x * 100) / float(total), 2))) + " %)\n")

def GetStat(str, x, total):
    s = str
    s += repr(x) + "   \t(" + GetPct(repr(round(float(x * 100) / float(total),2))) + " %)\n"
    return s

def GetPct(d):
    getcontext().prec = 3
    to_round = Decimal(d)
    str = repr(+to_round).rstrip().split('\'')
    return str[1]

def GetNum(d):
    getcontext().prev = 2
    to_round = Decimal(d)
    str = repr(+to_round).rstrip().split('\'')
    return str[1]
#"/home/jswaro/dumpfiles/logs/current/"

proc = subprocess.Popen(["ls", "/home/jswaro/dumpfiles/logs/previous/"], shell=False, stdout=subprocess.PIPE)
t = strftime("%m%d_%H:%M" , time.localtime())
for line in proc.stdout:
    subprocess.Popen(["cp", "/home/jswaro/dumpfiles/logs/previous/" + line.rstrip(),
        "/home/jswaro/dumpfiles/logs/old/" + t + line.rstrip()], shell=False)

file_assoc = {}

for line in sys.stdin:
    if line[0] != '!':
        split = line.rstrip().split()
        l = line.rstrip()
        type = split[1]
        if( type not in file_assoc ):
            file_assoc[type] = open("./logs/current/" + type + ".txt", "w")

        file_assoc[type].write(line)