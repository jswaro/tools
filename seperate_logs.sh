#!/bin/bash
TMP=$PWD
DEFAULT_FILE=valid.log

if [ "$#" -eq "1" ]
    then
        DEFAULT_FILE=$1
    else
        echo "Please specify only 1 argument as the file name"
        exit
fi

if [ ! -d "$TMP/logs/current" ]
    then
        mkdir $TMP/logs/current
fi
if [ ! -d "$TMP/logs/previous" ]
    then
        mkdir $TMP/logs/previous
fi
if [ ! -f "$PWD/$DEFAULT_FILE" ]
    then
        echo "Error: File $DEFAULT_FILE does not exist"
        exit
fi

cp $TMP/logs/current/* $TMP/logs/previous/
python logs.py < $DEFAULT_FILE
#python compare.py    #This line is only necessary is you are doing difference testing
exit
