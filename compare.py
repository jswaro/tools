from decimal import *
import os
from time import strftime
import sys
import time
import random
import commands
import shlex
import subprocess
import time
import exceptions

array = []

proc = subprocess.Popen(["ls", "/home/jswaro/dumpfiles/logs/current/"], shell=False, stdout=subprocess.PIPE)
for line in proc.stdout:
    try:
        cur_file = "/home/jswaro/dumpfiles/logs/current/" + line.strip()
        prev_file = "/home/jswaro/dumpfiles/logs/previous/" + line.strip()
        ret = subprocess.check_call(["diff", cur_file, prev_file], shell=False)
    except subprocess.CalledProcessError:
        array.append("Files: " + line.strip() + " differed...")

for line in array:
    print line
