#!/usr/bin/env python
# encoding: utf-8
'''
generate_dataset_samples -- shortdesc

generate_dataset_samples is a description

It defines classes_and_methods

@author:     James Swaro

@copyright:  2014 organization_name. All rights reserved.

@license:    license

@contact:    js311004@ohio.edu
@deffield    updated: Updated
'''

import sys
import os
import re
import random
import json

from optparse import OptionParser

__all__ = []
__version__ = 0.1
__date__ = '2014-06-15'
__updated__ = '2014-06-15'

DEBUG = 1
TESTRUN = 0
PROFILE = 0


OCTET = "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
IP_ADDR = OCTET + "\." + OCTET + "\." + OCTET + "\." + OCTET

tcprs_line = re.compile('^#< ' + IP_ADDR)
tcpcsm_line  = re.compile('^#> ' + IP_ADDR)
dump_line = re.compile('^/usr/sbin/tcpdump')

class ldr_event(object):
    def __init__(self):
        self.tcprs  = ""
        self.tcpcsm = ""
        self.statement = ""
        
    def key(self):
        '''
        #< 66.112.25.204 190.84.69.119 80 3075 FRETX 1075460157.616578
        #> 66.112.25.204 190.84.69.119 80 3075 10534 0 REORDER 1075460157.616578
        /usr/sbin/tcpdump -r tracefile -w outfile filter
        '''
        
        tcprs_fields = self.tcprs.split()
        tcpcsm_fields = self.tcpcsm.split()
        data_fields = self.statement.split()
        
        ip_id = "{6} {1} {2} {3} {4} {5}".format(*tcprs_fields)
        tcpcsm_classification = "UNDETECTED"
        if self.tcpcsm != "":
            tcpcsm_classification = tcpcsm_fields[7]
        input_file = data_fields[2]
        
        hk = "{0} {1} {2}".format(input_file, ip_id, tcpcsm_classification)
        
        return hk
    
    def get_tcpcsm_classification(self):
        classification = "UNDETECTED"
        tcpcsm_fields = self.tcpcsm.split()
        if self.tcpcsm != "":
            classification = tcpcsm_fields[7]
        
        return classification
    
    def get_input_file(self):
        data_fields = self.statement.split()
        return os.path.basename(data_fields[2])
    
    def get_output_file(self):
        data_fields = self.statement.split()
        return os.path.basename(data_fields[4])
    
    def get_tcpdump_filter(self):
        return " ".join(self.statement.split()[5:])
    
    def default(self):
        ret = {"tcprs": self.tcprs,
               "tcpcsm": self.tcpcsm,
               "tcpdump": self.statement,
               "files": {"input": self.get_input_file(),
                         "output": self.get_output_file()},
               "filter": self.get_tcpdump_filter(),
               "classification": self.get_tcpcsm_classification()
               }
        return ret
        

def parse_ldr_file(stream, rec_dict, filter):
    record = None
    for line in stream:
        clean = line.rstrip()
        data = clean.split()
        
        if tcprs_line.match(clean):
            if record is None:
                record = ldr_event()
            else:
                raise RuntimeError
            
            record.tcprs = clean
        elif tcpcsm_line.match(clean):
            record.tcpcsm = clean
        elif dump_line.match(clean):
            record.statement = clean
            
            hk = record.key()
            if not hk in rec_dict:
                if filter == "all" or filter == record.get_tcpcsm_classification():
                    rec_dict[record.key()] = record
            else:
                print >> sys.stderr, hk, rec_dict[hk], "duplicate"
                pass
            
            record = None
        
def choose_samples(records, samples):
    sample_set = dict()
    
    keys = records.keys()
    
    if len(keys) < samples:
        return records
    
    while len(sample_set) < samples:
        key = random.choice(keys)
        
        if key not in sample_set:
            sample_set[key] = records[key]
        else:
            continue
        
    
    return sample_set
    
def write_sample_set(stream, samples, dest):
    print >> stream, "#!/bin/bash"
    print >> stream, "#< event in tcprs"
    print >> stream, "#> event in tcpcsm"
    print >> stream, "# tcpdump command"
    print >> stream, "if [[ ! -d {0}/traces ]] ; then mkdir -p {0}/traces ; fi\n".format(dest)
    
    x = 0
    
    unique_infiles = list()
    
    for key in samples: 
        record = samples[key]
        
        print >> stream, record.tcprs
        print >> stream, record.tcpcsm 
        
        fields = record.statement.split()
        
        fdict = dict()
        fdict['infile']  = fields[2]
        fdict['dest']    = dest
        fdict['outfile'] = str(x).zfill(4) + ".dmp"
        fdict['filter']  = record.get_tcpdump_filter()
        
        if fdict["infile"] not in unique_infiles:
            unique_infiles.append(fdict["infile"])
            statement = "/usr/sbin/tcpdump -r {infile} -w {dest}/traces/{outfile} {filter}".format(**fdict)
        
            print >> stream, statement
            
        print >> stream, "\n"
        x += 1        

def main(argv=None):
    '''Command line options.'''

    program_name = os.path.basename(sys.argv[0])
    program_version = "v0.1"
    program_build_date = "%s" % __updated__

    program_version_string = '%%prog %s (%s)' % (program_version, program_build_date)
    #program_usage = '''usage: spam two eggs''' # optional - will be autogenerated by optparse
    program_longdesc = '''''' # optional - give further explanation about what the program does
    program_license = "Copyright 2014 James Swaro (organization_name)                                            \
                Licensed under the Apache License 2.0\nhttp://www.apache.org/licenses/LICENSE-2.0"

    if argv is None:
        argv = sys.argv[1:]

    # setup option parser
    parser = OptionParser(version=program_version_string, epilog=program_longdesc, description=program_license)
    parser.add_option("-v", "--verbose", dest="verbose", action="count", help="set verbosity level [default: %default]")
    parser.add_option("-c", "--count", dest="samples", action="count", help="set number of samples [default: %default]")
    parser.add_option("-f", "--filter", dest="filter", action="store", help="category filter [default: %default]")
    parser.add_option("-d", "--dir", dest="dir", action="store", help="location to store tcpdump files in trace generation file [default: %default]")
    parser.add_option("-w", "--writeto", dest="outputfile", action="store", help="location to store trace generation file [default: %default]")
    parser.add_option("-s", "--seed", dest="seed", action="store", help="set the random seed [default: %default]")
    
    parser.set_default("samples", 100)
    parser.set_default("filter", "all")
    parser.set_default("dir", os.path.abspath(os.path.join(os.curdir,"samples")))
    parser.set_default("outputfile", "gen_traces.sh")
    parser.set_default("seed", 0xDEADBEEF)

    # process options
    (opts, args) = parser.parse_args(argv)

    if opts.verbose > 0:
        print("verbosity level = %d" % opts.verbose)


    random.seed(opts.seed)

    # MAIN BODY #
    records = {}
    streams = []
    if len(args) > 0:
        streams = []
        for infile in args:
            if os.access(infile, os.R_OK):
                streams.append(infile)
                
        for inputfile in streams:
            with open(inputfile, 'r') as stream:
                parse_ldr_file(stream, records, opts.filter) 
    else:
        parse_ldr_file(sys.stdin, records, opts.filter)
        
    sample_set = choose_samples(records, opts.samples)
    
    assert(len(sample_set) == opts.samples or len(sample_set) == len(records))
    
    try:
        os.makedirs(opts.dir)
    except OSError:
        pass
    
    trace_file = os.path.abspath(os.path.join(opts.dir,opts.outputfile))

    dest_directory = os.path.abspath(os.path.expanduser(opts.dir)) 

    with open(trace_file, 'w') as trace_gen:
        write_sample_set(trace_gen, sample_set, dest_directory)
        
    os.chmod(trace_file, 755)

if __name__ == "__main__":
    if TESTRUN:
        import doctest
        doctest.testmod()
    if PROFILE:
        import cProfile
        import pstats
        profile_filename = 'generate_dataset_samples_profile.txt'
        cProfile.run('main()', profile_filename)
        statsfile = open("profile_stats.txt", "wb")
        p = pstats.Stats(profile_filename, stream=statsfile)
        stats = p.strip_dirs().sort_stats('cumulative')
        stats.print_stats()
        statsfile.close()
        sys.exit(0)
    sys.exit(main())