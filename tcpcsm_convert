#!/usr/bin/env python

'''
Created on May 17, 2013

@author: jswaro
'''

from tcpannotations.CongestionLogs import RetransmissionRecord,ReorderingRecord,\
    OptionsRecord
import sys
from optparse import OptionParser, make_option
from tcpannotations.TCPConfig import getConfigValue, getConfig
import os
import tcpannotations

CONFIGMODULE = "Summary";

class BroLog: 
    def __init__(self, filename):
        self.logfile = None;
        self.fields = None;
        self.header = ""
        
        try:
            self.logfile = open(filename, "r");
        except IOError as (errno, strerror):
            print "Warning: unable to open ", filename, " for processing...";
            self.logfile = None;
            return;
        
        try:
			
            self.header += self.logfile.readline(); #seperator line
            self.header += self.logfile.readline(); #set-separator line
            self.header += self.logfile.readline(); #empty-field line
            self.header += self.logfile.readline(); #unset field
            self.header += self.logfile.readline(); #path line
            self.header += self.logfile.readline(); #open line
            
            line = self.logfile.readline();
            self.fields = line.rstrip().split();
            self.header += line
            
            self.header += self.logfile.readline(); #types line
            
        except Exception as e:
            print e
            print "Unexpected error in processing log header";
            sys.exit(0);
        
    def good(self):
        return self.logfile != None

    def getFields(self):
        return self.fields;

    def readline(self):
        record = self.logfile.readline();
        if(record.split()[0] == "#close"):
            self.close();
        return record;
    
    def close(self):
        if (self.logfile != None):
            self.logfile.close();
        self.logfile = None;


class Connection:
    def __init__(self):
        self.bad = False;
        self.events = [];

def getConfigValues(configObj):
    global CONNLOG;
    global DEADLOG;
    global RETRANSMISSIONLOG;
    global OPTIONSLOG;
    global REORDERINGLOG;
    global SEARCHPATH;

    CONNLOG = getConfigValue(configObj, CONFIGMODULE, "connectionsummary");
    DEADLOG = getConfigValue(configObj, CONFIGMODULE, "deadconnlog");
    RETRANSMISSIONLOG = getConfigValue(configObj, CONFIGMODULE, "retransmissionlog");
    OPTIONSLOG = getConfigValue(configObj, CONFIGMODULE, "optionslog");
    REORDERINGLOG = getConfigValue(configObj, CONFIGMODULE, "reorderinglog");
    SEARCHPATH = getConfigValue(configObj, CONFIGMODULE, "defaultsearchpath");

def readOptionsLogFile( logfile, connections ):
    progress = 0;
    step = 10000;
    connections.clear();
    log = BroLog(logfile);
    
    record = log.readline();
    while(log.good()):

        record = OptionsRecord(record.split());
        conn = Connection();
        conn.bad = record.bad;
        
        connections[record.uid] = conn;
        
        record = log.readline();

    return connections;

def readLogFile(logfile, connections):
    if(logfile == None):
        return;
    progress = 0;
    step = 10000;
    labelIndex = None;

    try:
        log = BroLog(logfile);
        data = log.getFields();

        for i in range(0, len(data)):
            if(data[i].rstrip() == "label"):
                labelIndex = i - 1;
                break;

        if( labelIndex == None ):
            raise Exception("Could not find the label index");

        record = log.readline();
        while(log.good()):
            fields = record.rstrip().split();
            uid = fields[1]; #make this into a const defined somewhere
            src = fields[2];

            connections[uid].ProcessLine(record, labelIndex, False);
            
            record = log.readline();
            
        log.close();
    except IOError as (errno, strerror):
        print "Skipping ", logfile, " event processing..."


def print_retransmission_record(record):
    return ""

def print_reordering_record(record):
    flow_id = "{0} {1} {2} {3}".format(record.resp_h, record.orig_h,
                                       record.resp_p, record.orig_p);
        
    return "{0} REORDER {1}".format(flow_id,
                                    record.timestamp);
        

def print_record(record_type, record):
    if (record_type == tcpannotations.CongestionLogs.RetransmissionRecord):
        return print_retransmission_record(record);
    return print_reordering_record(record);

def init_parser():
    usage="usage: %prog [options] logfile"
    options_list = [];
    parser = OptionParser(usage=usage, option_list=options_list);
    return parser;

def main():
    parser = init_parser();
    
    (options, args) = parser.parse_args();
    
    tcpconfig = getConfig();
    getConfigValues(tcpconfig);
    
    connections = {};
    
    if(len(args) == 0):
        parser.print_help();
        sys.exit(-1);
        
    basedir = os.path.dirname(args[0]);
    readOptionsLogFile(basedir + os.sep + OPTIONSLOG, connections);
    
    #assume the tcpoptions file is in the same location as the other log file
    for filename in args:
        
        log = BroLog(filename);

        label_index = log.fields.index("label") - 1;
        
        print log.header.rstrip();
        
        line = log.readline()
            
        while(log.good()):
            label = line.split()[label_index];
        
            cls = None;
            if (label in ["TCP::Reordering"]):
                cls = ReorderingRecord;
            else:
                cls = RetransmissionRecord;
            
            rec = cls(line.split());
            
            
            #don't print records from bvad connections
            if (not connections[rec.uid].bad):
				print line.rstrip();

            line = log.readline();
            
        print "#close"
        

if __name__ == '__main__':
    main();
