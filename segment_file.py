from decimal import *
import os
import sys
import time
import random
import subprocess
import getopt

def usage():
    print "Usage: segment_file.py -i infile [-s #] [-o]"
    print "\t-h, --help \t\tprovides this help menu";
    print "\t-i \t\tspecifies the file to read from";
    print "\t-s \t\tSegmentation size based on lower 8 bit boundaries, Default: 32";


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:s:", ["help", "input", "seg"]);
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err); # will print something like "option -a not recognized"
        usage();
        sys.exit(2);
    infile = None;
    outfile = None;
    segment_size = 32
    for o, a in opts:
        if o in ("-h", "--help"):
            usage();
            sys.exit();
        elif o in ("-i", "--input"):
            infile = a;
        elif o in ("-o", "--output"):
            outfile = a;
        elif o in ("-s", "--seg"):
            segment_size = int(a);
        else:
            assert False, "unhandled option";

    if( infile == None ):
        usage();
        sys.exit(2);

    file_to_segment = infile

    shell_script = open("pcap_segmentation.sh", "w")

    for x in range(segment_size -1 , 256, segment_size):
        print repr(x) + " - " + repr(x - segment_size + 1);
        shell_script.write("tcpdump -n '((ip[15]>=" + repr(x - segment_size + 1) +
        " and ip[15]<=" + repr(x) + ") or (ip[19]>=" + repr(x - segment_size + 1) +
        " and ip[19]<=" + repr(x) + "))' -r " + file_to_segment + ".dmp -w " +
        file_to_segment + "_" + repr(x - segment_size + 1) +".dmp\n");





__author__ = "James Swaro";
__date__ = "June 16th, 2011";
if __name__ == "__main__":
    main();