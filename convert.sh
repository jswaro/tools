#!/bin/bash
LOG_FILE=rto_test.log

if [ "$#" -eq "0" ]
    then
        echo "Usage: "
        echo "          Just pass the target directory containing the dump files"
        echo "          to convert as the argument to this script. This doesn't "
        echo "          script does not try to differentiate file types.        "
        exit
fi

for file in $PWD/$1/*
do
  if [ ! -d "$file" ]
    then
	FILE_NAME=$(basename $file)
	echo Processing $file ...
      	(tcptrace -n -O$FILE_NAME.dmp $file )
	wait
  fi
done

exit
