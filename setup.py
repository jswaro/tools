#! /usr/bin/env python

from distutils.core import setup

setup(name='tcpannotations',
      version="0.8", # Filled in automatically.
      description='A set of scripts for annotating TCP traces and summarizing \
        them',
      author='James Swaro',
      author_email='james.swaro@gmail.com',
      url='http://bitbucket.org/jswaro/tools',
      scripts=['annotate', 'summarize', 'readtcplog', 'sampleTraces', 'tcpcsm_convert', 'filter_bad_records', 'generate_dataset_samples.py'],
      packages=['tcpannotations'],
      package_dir={'tcpannotations': 'src/tcpannotations'},
     )
