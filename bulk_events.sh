#!/bin/bash
LOG_FILE=rto_test.log

for file in $PWD/$1/*
do
  if [ ! -d "$file" ]
    then
	FILE_NAME=$(basename $file)
	echo Processing $file ...
	if [ ! -d "$PWD/$1/logs" ]
          then
            mkdir $PWD/$1/logs
        fi
      	(bro -Cr $file rto.bro &> /dev/null)
	wait
	cp $PWD/$LOG_FILE $PWD/$1/logs/"$FILE_NAME"_results.log
  fi
done

exit
